const Order = require("./order");

const OrderState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    TYPE : Symbol("type"),
    SIZE:   Symbol("size"),
    TOPPINGS:   Symbol("toppings"),
    UPSELL:  Symbol("upsell"),
    DONE : Symbol("done")
});

const types = [{type:"Banquet burger", price : 3.5}, {type:"Barbecue burger", price : 2.5},{type : "Buffalo burger", price : 4}];
const sizes = ["Medium", "Large"];
const tax = 13;
class BurgerOrder extends Order{
    constructor(){
        super();
        this.stateCur = OrderState.WELCOMING;
        this.type = [];
        this.size = [];
        this.addOn ="";
        this.toppings = [];
        this.details = [];
    }
    handleInput(sInput, anotherItem){
        switch(this.stateCur){
            case OrderState.WELCOMING:
                this.stateCur = OrderState.TYPE;
                this.details.push("Welcome to Bhupinder's Burger joint.");
                this.details.push("What would you like?");
                for(let i =0; i < types.length ; i++){
                    this.details.push(`${i+1}. ${types[i].type}`);
                }
                this.details.push("Please enter 1, 2 or 3 only");
                break;
            case OrderState.TYPE:
                this.details = [];
                if(sInput.toLowerCase() == "no"){
                    this.stateCur = OrderState.TOPPINGS; 
                    this.handleInput(sInput, true);
                }
                else{
                    if(!types[sInput-1])
                    {
                        this.details.push("Please enter 1, 2 or 3 only");
                    }
                    else{
                        this.type.push(sInput-1);
                        this.stateCur = OrderState.SIZE;
                        this.details.push("What size would you like?");
                        for(let i =0; i < sizes.length ; i++){
                            this.details.push(`${i+1}. ${sizes[i]}`);
                        }
                        this.details.push("Please enter 1 or 2 only");
                    } 
                }                               
                break;
            case OrderState.SIZE:
                this.details = [];
                if(!sizes[sInput-1]){
                    this.details.push("Please enter 1 or 2 only");
                }
                else{
                    this.stateCur = OrderState.TOPPINGS; 
                    this.size.push(sInput -1);
                    this.details.push("Would you toppings would you like to have?");
                }
                break;
            case OrderState.TOPPINGS:
                this.details = [];
                if(!anotherItem){
                    this.stateCur = OrderState.TYPE;
                    this.toppings.push(sInput);
                    this.details.push(`What else would you like to have?`);
                    for(let i =0; i < types.length ; i++){
                        this.details.push(`${i+1}. ${types[i].type}`);
                    }
                    this.details.push("Please enter 1, 2 or 3");
                    this.details.push(`Enter "No" if do not want to order anything else`);
                }
                else{
                    this.stateCur = OrderState.UPSELL;
                    this.details.push(`Would you also like to have cheese loaded fries? (Yes/No)`);
                }
                break;
            case OrderState.UPSELL:
                this.isDone(true);
                this.details = [];
                let subTotal =0;
                let order = '';
                for(let i =0 ; i < this.type.length ; i++){
                    let burger = types[this.type[i]];
                    subTotal += burger.price;
                    order += `${sizes[this.size[i]]} ${burger.type} with ${this.toppings[i]} \n`;
                }
                this.details.push(order);
                if(sInput.toLowerCase() === "yes"){
                    this.addOn = "Cheese loaded fries";
                }
                let addOnPrice = 0;
                if(this.addOn){
                    this.details.push(this.addOn);
                    addOnPrice = 1.5;
                }
                let d = new Date(); 
                d.setMinutes(d.getMinutes() + 20);
                subTotal += addOnPrice;
                let taxAmount = subTotal * tax * .01;
                let total = subTotal + taxAmount;
                this.details.push(`Your sub total is $${subTotal.toFixed(2)}`)
                this.details.push(`Tax is ${tax}% and tax amount is $${taxAmount.toFixed(2)}`);
                this.details.push(`Your total amount is $${total.toFixed(2)}`);
                this.details.push(`Please pick it up at ${d.toTimeString()}`);
                break;
        }
        return this;
    }
}


module.exports =  { BurgerOrder, OrderState}